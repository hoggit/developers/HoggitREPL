package HoggitREPL

import (
	"bufio"
	"fmt"
	"net"
	"sync"
	"time"
)

func ResetConnection(address string) *net.UDPConn {
	s, _ := net.ResolveUDPAddr("udp4", address)
	conn, _ := net.DialUDP("udp4", nil, s)
	return conn
}

func Connect(prefixState *LivePrefixState, comms *chan string) {
	var mizConn *net.UDPConn
	var ggConn *net.UDPConn
	mizConnected := false
	ggConnected := false
	var mizStatusMux sync.Mutex
	var mizConnMux sync.Mutex

	var ggStatusMux sync.Mutex
	var ggConnMux sync.Mutex

	go func() {
		for {
			mizStatusMux.Lock()
			if !mizConnected {
				mizConnMux.Lock()
				if mizConn != nil {
					err := mizConn.Close()
					if err == nil {
						fmt.Println("Connection was closed.\n")
					}
				}

				mizConn = ResetConnection(prefixState.Address + ":15301")
				err := mizConn.SetReadDeadline(time.Now().Add(5 * time.Second))
				if err != nil {
					fmt.Println(err)
					time.Sleep(5 * time.Second)
					continue
				}

				_, err = mizConn.Write([]byte("connect"))
				if err != nil {
					fmt.Println(err)
					time.Sleep(5 * time.Second)
					continue
				}

				_, err = bufio.NewReader(mizConn).ReadString('\000')
				if err != nil {
					fmt.Println(err)
					time.Sleep(5 * time.Second)
					continue
				}
				mizConnMux.Unlock()

				mizConnected = true
				go func() {
					for {
						if !mizConnected {
							return
						}

						mizConnMux.Lock()
						mizConn.Write([]byte("HEARTBEAT"))
						mizConnMux.Unlock()
						time.Sleep(5 * time.Second)
					}
				}()
			}
			mizStatusMux.Unlock()

			ggStatusMux.Lock()
			if !ggConnected {
				ggConnMux.Lock()
				if ggConn != nil {
					err := ggConn.Close()
					if err == nil {
						fmt.Println("Connection was closed.\n")
					}
				}

				ggConn = ResetConnection(prefixState.Address + ":15302")
				err := ggConn.SetReadDeadline(time.Now().Add(5 * time.Second))
				if err != nil {
					fmt.Println(err)
					time.Sleep(5 * time.Second)
					continue
				}

				_, err = ggConn.Write([]byte("connect"))
				if err != nil {
					fmt.Println(err)
					time.Sleep(5 * time.Second)
					continue
				}

				_, err = bufio.NewReader(ggConn).ReadString('\000')
				if err != nil {
					fmt.Println(err)
					time.Sleep(5 * time.Second)
					continue
				}
				ggConnMux.Unlock()

				ggConnected = true
				go func() {
					for {
						if !ggConnected {
							return
						}

						ggConnMux.Lock()
						ggConn.Write([]byte("HEARTBEAT"))
						ggConnMux.Unlock()
						time.Sleep(5 * time.Second)
					}
				}()
			}
			ggStatusMux.Unlock()

			message := <- *comms
			if len(message) == 0 {
				*comms <- "\n"
				continue
			}

			var activeConn *net.UDPConn

			if prefixState.Environment == "Mission" {
				activeConn = mizConn
			} else if prefixState.Environment == "GameGUI" {
				activeConn = ggConn
			}

			if message == "disconnect" {
				mizConn.Write([]byte("GOODBYE"))
				ggConn.Write([]byte("GOODBYE"))
				return
			}

			_, err := activeConn.Write([]byte(message))
			if err != nil {
				fmt.Println(err)
				*comms <- "failed to write to DCS"
				mizStatusMux.Lock()
				ggStatusMux.Lock()
				mizConnected = false
				ggConnected = false
				mizStatusMux.Unlock()
				ggStatusMux.Unlock()
				continue
			}

			activeConn.SetReadDeadline(time.Now().Add(5 * time.Second))
			response, err := bufio.NewReader(activeConn).ReadString('\000')
			if err != nil {
				fmt.Println(err)
				*comms <- err.Error()
				mizStatusMux.Lock()
				ggStatusMux.Lock()
				mizConnected = false
				ggConnected = false
				mizStatusMux.Unlock()
				ggStatusMux.Unlock()
				continue
			}
			*comms <- response
		}
	}()
}
