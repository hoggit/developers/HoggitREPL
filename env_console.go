package HoggitREPL

import (
	"fmt"
	"github.com/c-bata/go-prompt"
)

func Env_executor(t string) {
	switch t {
	case "disconnect":
		*activeComm <- t
		livePrefixState.Environment = "DISCONNECTED"
		Main_console.Run()
	default:
		*activeComm <- t
		message := <- *activeComm
		fmt.Print(message + "\n")
	}
}

func Env_completer(d prompt.Document) []prompt.Suggest {
	s := []prompt.Suggest {
		{Text: "disconnect", Description: "Return to main menu"},
	}

	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}